@echo off
setlocal

REM Deal with folders, try to understand where we are...
SET scriptfolder=%~dp0
FOR %%a IN ("%scriptfolder:~0,-1%") DO SET rootfolder=%%~dpa
IF %rootfolder:~-1%==\ SET rootfolder=%rootfolder:~0,-1%

REM Set local variables
SET zipfilename=reyt.zip
SET abszipfilename=%rootfolder%\Release\%zipfilename%
SET zipcommand=%rootfolder%\Release\7za

REM Remove previous release, if any.
del /f /q %abszipfilename% > nul 2>&1

REM Speak to the user
ECHO ----------------------------------------------
ECHO   Generating the %zipfilename% archive
ECHO ----------------------------------------------

REM Process the CHANGELOG to a more human readable text format
REM First create a copy of the original CHANGELOG
COPY %rootfolder%\CHANGELOG.md %TEMP%\Changelog.txt > nul
REM Replace ** by nothing
powershell -Command "(Get-Content '%TEMP%\Changelog.txt').replace('**','') | Out-File -encoding ASCII %TEMP%\Changelog.txt"
REM Replace <sup>X</sup> by *
powershell -Command "(Get-Content '%TEMP%\Changelog.txt').replace('<sup>X</sup>', '*') | Out-File -encoding ASCII %TEMP%\Changelog.txt"

REM Create the archive
%zipcommand% a -tzip %abszipfilename% %rootfolder%\SectorFiles\*.isc %rootfolder%\SectorFiles\Include %rootfolder%\ColorSchemes\*.clr %TEMP%\Changelog.txt
REM Move the Changelog into the correct folder
%zipcommand% rn %abszipfilename% Changelog.txt Include\RE_YT\Changelog.txt > nul

REM Delete the temporary text Changelog
DEL %TEMP%\Changelog.txt

REM Let the user understand what just damn happened
ECHO.
PAUSE

REM And quit! Love <3