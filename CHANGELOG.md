# Aurora Reunion Changelog #

## Update 29 January 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **FIXES files (fix)** : REYT
- **VOR files (vor)** : REYT
- **NDB files (ndb)** : REYT
- **AWY files (hawy/lawy)** : REYT
- **RWY files (rwy)** : REYT
- **SID files (sid)** : FMEE
- **STAR files (str)** : FMCZ, FMEE, FMEP
- **GND files (gnd)** : FMEE, FMEP, 9741, 9762
- **Taxiway label files (txi)** : FMEP
- **Gates label files (gts)** : FMEE, FMEP
- **VFR fixes files (vfi)** : FMEP
- **Danger files (danger)** : FMEE
- **Restricted files (restrict)** : FMEE
- **Colorscheme (clr)** : FRANCE

---
### Added Files ###

- **Symbols file (sym)** : FR

---
---
## Update 22 February 2023 ##
**{+ AIRAC : 2402 +}**

### Updated Files ###

- **FIXES files (fix)** : REYT
- **AWY files (hawy/lawy)** : REYT

---
---
## Update 27 May 2023 ##
**{+ AIRAC : 2305 +}**

### Updated Files ###

- **CCR files (artcc)** : FIMM, FMMM
- **TMA files (hartcc)** : FIMP
- **STAR files (str)** : FMEP
- **GND files (gnd)** : FMEE, FMEM, FMEN, FMEP, FMES
- **Gates label files (gts)** : FMEE, FMEP
- **Danger files (danger)** : FMEE

---
### Added Files ###

- **FIXES files (fix)** : REYT<sup>X</sup>
- **NDB files (ndb)** : REYT<sup>X</sup>
- **AWY files (hawy/lawy)** : REYT<sup>X</sup>
- **APT files (apt)** : REYT<sup>X</sup>
- **RWY files (rwy)** : REYT
- **STAR files (str)** : FMCZ
- **GND files (gnd)** : 9741, 9743, 9745, 9761, 9762, FMCZ
- **Taxiway label files (txi)** : FMCZ
- **Gates label files (gts)** : FMCZ
- **VFR fixes files (vfi)** : FMCZ
- **Danger files (danger)** : FMCH
- **Restricted files (restrict)** : FMCZ
- **TMA files (hartcc)** : FMCH
- **CTR files (lartcc)** : FMCH
- **ATC File (atc)** : FMCZ
- **GEO files (geo)** : FMMM, FIMM
- **ATIS file (atis)** : France

---
### Deleted Files ###

- **FIXES files (fix)** : FMEE<sup>X</sup>
- **NDB files (ndb)** : FMEE<sup>X</sup>
- **AWY files (hawy/lawy)** : FMEE<sup>X</sup>
- **GEO files (geo)** : FMEE<sup>X</sup>
- **RWY files (rwy)** : FMEE<sup>X</sup>
- **APT files (apt)** : FMEE<sup>X</sup>


<sup>X</sup> : Inclusion of Mayotte sector needed a file structure change. Old specific FMEE files have been merged into bigger files covering both Mayotte and La Reunion airports. That's why some FMEE file are listed as deleted, and some new FMMM/FIMM/REYT file are listed as created.

---
---
## Update 14 July 2022 ##
**{+ AIRAC : 2207 +}**

### Updated Files ###

- **RWY files (rwy)** : FMEE
- **FIXES files (fix)** : FMEE
- **VOR files (vor)** : FMEE
- **NDB files (ndb)** : FMEE
- **AWY files (hawy/lawy)** : FMEE
- **CCR files (artcc)** : FIMM, FMMM
- **TMA files (hartcc)** : FMEE
- **SID files (sid)** : FMEE, FEMP
- **STAR files (str)** : FMEE, FMEP
- **APT files (apt)** : FMEE
- **GND files (gnd)** : FMEE, FMEP
- **Gates label files (gts)** : FMEE, FMEP
- **GEO files (geo)** : FMEE
- **Danger files (danger)** : FMEE
- **Restricted files (restrict)** : FMEE

---
### Added Files ###

- **CTR files (lartcc)** : FIMP
- **GND files (gnd)** : 9742, 9744, FMEH, FMEL, FMEM, FMEN, FMES
- **Gates label files (gts)** : 9742, FMEH, FMEL, FMEM

---
---
## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**No Applicable Updates to date**

---
---
## Update 17 November 2020 ##
**{+ AIRAC : 2012 +}**

**No Applicable Updates to date**

---
---
## Update 10 October 2020 ##
**{+ AIRAC : 2011 +}**

**No Applicable Updates to date**

---
---
## Update 19 September 2020 ##
**{+ AIRAC : 2010 +}**

### Updated Files ###

- **APT files (apt)** : FMEE

---
---
## Update 11 July 2020 ##
**{+ AIRAC : 2007 +}**

### Updated Files ###

- **CCR files (artcc)** : FIMM, FMMM

---
---
## Update 30 May 2020 ##
**{+ AIRAC : 2006 +}**

### Updated Files ###

- **STAR files (str)** : FMEE, FMEP
- **SID files (sid)** : FMEE, FMEP
